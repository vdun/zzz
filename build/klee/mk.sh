sudo sh -c 'echo "deb http://llvm.org/apt/precise/ llvm-toolchain-precise-3.4 main" >> /etc/apt/sources.list.d/llvm.list'
sudo sh -c 'echo "deb-src http://llvm.org/apt/precise/ llvm-toolchain-precise-3.4 main" >> /etc/apt/sources.list.d/llvm.list'
sudo sh -c 'echo "deb http://llvm.org/apt/precise/ llvm-toolchain-precise-3.5 main" >> /etc/apt/sources.list.d/llvm.list'
sudo sh -c 'echo "deb-src http://llvm.org/apt/precise/ llvm-toolchain-precise-3.5 main" >> /etc/apt/sources.list.d/llvm.list'
    # Needed for CMake
sudo add-apt-repository -y ppa:ubuntu-sdk-team/ppa
    # Needed for new libstdc++ and gcc4.8
sudo add-apt-repository --yes ppa:ubuntu-toolchain-r/test/
wget -O - http://llvm.org/apt/llvm-snapshot.gpg.key|sudo apt-key add -
sudo apt-get update
    ###########################################################################
    # Set up out of source build directory
    ###########################################################################
# export KLEE_SRC=`pwd`
export KLEE_SRC=$pthOrg/$prj
export LLVM_VERSION="3.4"
export KLEE_UCLIBC=1
export STP_VERSION=master
cd $KLEE_SRC
cd ../
mkdir build
cd build/
export BUILD_DIR=`pwd`
    ###########################################################################
    # Install stuff
    ###########################################################################
sudo apt-get install gcc-4.8 g++-4.8 libcap-dev cmake
    # Make gcc4.8 the default gcc version
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 20
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 20
    # Make Clang3.4 the default clang version
sudo apt-get install clang-3.4
sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-3.4 20
sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-3.4 20
    # Install LLVM and the LLVM bitcode compiler we require to build KLEE
${KLEE_SRC}/.travis/install-llvm-and-runtime-compiler.sh
    # Install lit (llvm-lit is not available)
sudo pip install lit
    # Build STP
mkdir stp
cd stp
${KLEE_SRC}/.travis/stp.sh
cd ../

    # Get needed utlities/libraries for testing KLEE
mkdir test-utils/
cd test-utils/
${KLEE_SRC}/.travis/testing-utils.sh
cd ../
    # Build KLEE
${KLEE_SRC}/.travis/klee.sh

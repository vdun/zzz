#!/usr/bin/env bash
set -x
python setup.py install
cp /usr/local/lib/python2.7/dist-packages/amoco-*.egg ./amoco-py2.7.egg
ls -1 | grep -v 'mk.sh' | grep -v 'egg' | xargs -I xxx rm -rf xxx
# cp /home/ubuntu/virtualenv/python2.7/lib/python2.7/site-packages/amoco*.egg ./amoco.egg

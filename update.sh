#!/usr/bin/env bash
chmod 777 ./mk.sh
sSed="sed 's|^[- ]||gi'"
git submodule 
git submodule | eval $sSed | cut -d ' ' -f1,2 | sort  > /tmp/cVer.txt
for pkg in $(comm ./mVer.txt /tmp/cVer.txt -31 | cut -d ' ' -f2); do
	echo "___________________________________________["$pkg"]"
	./mk.sh $pkg
	sed -i "/$pkg/d" ./mVer.txt
	git submodule status $pkg | eval $sSed | cut -d ' ' -f1,2 >> ./mVer.txt
	sort ./mVer.txt > /tmp/mVer.txt; mv /tmp/mVer.txt ./mVer.txt
	
	git add --all
	git commit -m "ver_${pkg}_`date '+%Y-%m-%d-%H:%M'`"
	git push origin
done
# git submodule | sort > ./mVer.txt
